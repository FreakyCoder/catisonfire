﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour {
	private int currentScore;

	// Use this for initialization
	void Start () {
		currentScore = 0;
	}

	public void Collect(Collectible collectible) {
		currentScore += collectible.points;
	}
}