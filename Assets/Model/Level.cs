﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level {

	private int levelID;
	private int levelScore;
	public int LevelID {
		get {
			return this.levelID;
		}
		set {
			levelID = value;
		}
	}

	public int LevelScore {
		get {
			return this.levelScore;
		}
		set {
			levelScore = value;
		}
	}

	public Level (int levelID, int levelScore)
	{
		this.levelID = levelID;
		this.levelScore = levelScore;
	}
	





}
