﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {
	public GameObject prefab;
	public AudioClip clip;

	// Use this for initialization
	void Start () {
		var music = GameObject.Find ("Music");
		if (music == null) {
			music = Instantiate(prefab);
			music.name = "Music";
		}

		var musicControl = music.GetComponent<MusicControl> ();

		/*
		if (clip != null && musicControl.CurrentlyPlaying != clip) {
			musicControl.play (clip);
		}
		*/
	}
}