﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
	public long id;
	public string name;
	public int score;

	public static Player Current { get; set; }
}