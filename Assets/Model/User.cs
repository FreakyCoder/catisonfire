﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User {

	private string username;
	private string eMail;
	private int music;
	private int volume;
	private string language;
	private List<Level> levelList;

	public string Username {
		get {
			return this.username;
		}
		set {
			username = value;
		}
	}

	public string EMail {
		get {
			return this.eMail;
		}
		set {
			eMail = value;
		}
	}

	public int Music {
		get {
			return this.music;
		}
		set {
			music = value;
		}
	}

	public int Volume {
		get {
			return this.volume;
		}
		set {
			volume = value;
		}
	}

	public string Language {
		get {
			return this.language;
		}
		set {
			language = value;
		}
	}

	public List<Level> LevelList{
		get{
			return this.levelList;
		} set{
			levelList = new List<Level>();
		}
	}


	public User (string username, string eMail, int music, int volume, string language, List<Level> levelList)
	{
		this.username = username;
		this.eMail = eMail;
		this.music = music;
		this.volume = volume;
		this.language = language;
		this.levelList = levelList;
	}

	public User(){
		levelList = new List<Level>();
	}

}
