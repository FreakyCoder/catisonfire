﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownScript : MonoBehaviour {

	public float countdownTimer;	// Preparation Phase
	public float roundTimer;		// Main Game Phase


	private Text timerText;
	public static GameObject mainBall;

	// Initialization of timer and text
	void Start () {
		timerText = GetComponent<Text> ();
		mainBall = GameObject.Find("AsteroidSprite"); // AsteroidSprite is assigned
	}

	// Update is called once per frame
	void Update ()
	{
		var ballScript = mainBall.GetComponent<MainBallScript> ();
		if (countdownTimer < 0 && GameAliveScript.isGameAlive == 0) { //when timer ends game begins
			GameAliveScript.isGameAlive = 1;
			ballScript.rb2d.gravityScale = 1;
			ballScript.rb2d.isKinematic = false;
			ballScript.rb2d.freezeRotation = false;
			//ballScript.rb2d.velocity = new Vector2 (0f, 0f);
			ballScript.enabled = false;
			//GetComponent<CountdownScript> ().enabled = false;
		} else if (countdownTimer > 0 && GameAliveScript.isGameAlive == 0) { //untill timer ends show it on screen
			countdownTimer -= Time.deltaTime;
			timerText.text = countdownTimer.ToString ("f0");
		}

		if (GameAliveScript.isGameAlive == 1) { //round countdown
			roundTimer -= Time.deltaTime;
			Color color = new Color ();
			ColorUtility.TryParseHtmlString ("#C70000", out color);
			timerText.color = color;
			timerText.text = roundTimer.ToString ("f0");
		}
	}

	public float getRoundTimer(){
		return roundTimer;
	}

}
