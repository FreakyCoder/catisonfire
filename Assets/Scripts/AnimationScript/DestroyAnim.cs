﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAnim : MonoBehaviour {

	public static GameObject anim;

	// Use this for initialization
	void Start () {
		anim = GameObject.Find ("LaserAnim");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag ("Bowling")) {
			Destroy (anim);
		}
	}
}
