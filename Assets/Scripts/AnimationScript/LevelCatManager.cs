using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelCatManager : MonoBehaviour
{

	Animator anim;
	public float speed;
	public Rigidbody2D rb;
	public static GameObject levelBall;
	public GameObject Popup_GameClear;
	private GameObject scoreHolder;
	private GameObject scoreText;


	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();
		levelBall = GameObject.Find ("AsteroidSprite");
		anim = GetComponent<Animator> ();
		scoreHolder = GameObject.Find ("ScoreHolder");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (levelBall.transform.position.y < -4.747599) {
			rb.transform.Translate (speed * Time.deltaTime, 0, 0);
			anim.SetInteger ("State", 1);
		} 
		if (speed == 0) {
			anim.SetInteger ("State", 0);
		}
	}

	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.gameObject.CompareTag ("Ball")) {
			var scoreScript = scoreHolder.GetComponent<ScoreScript> ();
			int score = scoreScript.calculateScore (true);

			Debug.Log ("Success Score : " + score);

			other.gameObject.SetActive (false);

			speed = 0;
			GameAliveScript.isGameAlive = 0;
			Time.timeScale = 0;
			Popup_GameClear.SetActive (true);
			scoreText = GameObject.Find ("LevelScore");
			var scoreTextScript = scoreText.GetComponent<SetScoreTextScript> ();
			scoreTextScript.setScoreText (score);
			GameObject wonScreen = GameObject.Find ("won");
			var wonScript = wonScreen.GetComponent<SetCompleteStarScript> ();
			wonScript.setCompletedStar ();

			Debug.Log ("Success Score 2: " + score);
			insertCompletedLevel (score);
		}
		
		if (levelBall.transform.position.y < -4) {
			rb.transform.Translate (speed * Time.deltaTime, 0, 0);
		}
	}


	void insertCompletedLevel (int level_score)
	{

		Debug.Log ("Success Score 3: " + level_score);

		string url = "http://freakycoder.net/php/levelScore.php";

		string user_id = PlayerPrefs.GetString (Constants.PLAYER_ID);
		int levelID = PlayerPrefs.GetInt (Constants.CURRENT_LEVELID);
		//int level_score = PlayerPrefs.GetInt(Convert.ToString(levelID));

		Debug.Log ("Post Level Completed : ");
		Debug.Log ("UserID : " + user_id + "\nLevelID : " + levelID);

		Debug.Log ("Success Score 4: " + level_score);
		WWWForm form = new WWWForm ();
		// Sending the POST values
		form.AddField ("user_id", user_id);
		form.AddField ("level_id", levelID);
		form.AddField ("level_score", level_score);
		// Sending the request
		WWW www = new WWW (url, form);
		// Response from the server
		StartCoroutine (responseWWW (www));
	}

	IEnumerator responseWWW (WWW www)
	{

		yield return www;
		Debug.Log ("LevelCompleted Value : " + www.text);

		yield break;
	}

		
}
