﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseAndResume : MonoBehaviour {

	public bool pause;
	// Use this for initialization
	void Start () {
		pause = false;
	}
	// Handling pause button
	public void onPause(){
		pause = !pause;
		if (!pause) {
			Time.timeScale = 1;
		} else if (pause) {
			Time.timeScale = 0;
		}
	}
}
