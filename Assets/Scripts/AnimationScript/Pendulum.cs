﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour {

	// Public Variables
	public Rigidbody2D body2d;
	public float leftPushRange;
	public float rightPushRange;
	public float velocityHold;
	 // End Public Variables



	// Use this for initialization
	//Main Methods
	void Start () {
		body2d = GetComponent<Rigidbody2D> ();
		body2d.angularVelocity = velocityHold;
	}
	
	// Update is called once per frame
	void Update () {
		Push ();
	}
	//End Main Methods

	// Utility Methods
	public void Push(){
		if (transform.rotation.z > 0
		    && transform.rotation.z < rightPushRange
		    && (body2d.angularVelocity > 0)
		    && body2d.angularVelocity < velocityHold) {

			body2d.angularVelocity = velocityHold;
		} else if (transform.rotation.z < 0
		        && transform.rotation.z > leftPushRange
		        && (body2d.angularVelocity < 0)
		        && body2d.angularVelocity > velocityHold * -1) {
		
			body2d.angularVelocity = velocityHold * -1;
		}
	}

	// End Utility Methods
}
