﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineScript : MonoBehaviour {

	public static GameObject levelBall;
	public static Rigidbody2D rb2d;
	public int pushForceUp;
	public int pushForceLeft;
	public int pushForceRight;

	// Use this for initialization
	void Start () {
		levelBall = GameObject.Find ("AsteroidSprite");
		rb2d = levelBall.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag ("Ball")) {
			Debug.Log ("collisiontrampoline");
			rb2d.AddForce(Vector2.up*pushForceUp);
			rb2d.AddForce (Vector2.right * pushForceRight);
			rb2d.AddForce (Vector2.left * pushForceLeft);
		}
	
	}
}
