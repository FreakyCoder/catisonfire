﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraAspectControl : MonoBehaviour {
	double orthographicSize = 7;
	public void setAspect(int screenOri){


		float height = (float)Screen.height;
		float width = (float)Screen.width;


		Debug.Log ("Screen Width : " + width);
		Debug.Log ("Screen Height : " + height);

		if (screenOri == 1) {
			float widthTrue = width;
			float heightTrue = height;
			if (height > width) {
				widthTrue = height;
				heightTrue = width;
			}
			float ratio = widthTrue / heightTrue;
			Debug.Log ("ORIENTATION : Landscape : " + ratio );
			if (ratio >= 1.7) {
				Debug.Log ("9:16");
				orthographicSize = 7;
			} else if (ratio >= 1.5) {
				Debug.Log ("2:3");
				orthographicSize = 8.44;
			} else {
				Debug.Log ("3:4");
				orthographicSize = 5.62;
			}
		} else {
			float widthTrue = width;
			float heightTrue = height;
			if (height < width) {
				widthTrue = height;
				heightTrue = width;
			}
			float ratio = widthTrue / heightTrue;
			Debug.Log ("ORIENTATION : Portrait : " + ratio);
			if (ratio >= 0.56) {
				Debug.Log ("16:9");
				orthographicSize = 5.1;
			} else if (ratio >= 0.66) {
				Debug.Log ("3:2");
				orthographicSize = 6.85;
			} else {
				Debug.Log ("4:3");
				orthographicSize = 7.7;
			}
		}

		Camera.main.orthographicSize = (float)orthographicSize;
	}
}
