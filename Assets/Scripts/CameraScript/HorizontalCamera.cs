﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalCamera : MonoBehaviour {

	public bool makeHorizontal;

	// Use this for initialization
	void Awake () {

		Debug.Log ("Camera Size : " + Camera.main.orthographicSize);
		var aspectScript = GetComponent<CameraAspectControl> ();

		if (makeHorizontal == true) {
			Screen.orientation = ScreenOrientation.Landscape;
			Debug.Log ("HorizontalCamera Ori1 : " + Screen.orientation.ToString());
			aspectScript.setAspect (1);
			Debug.Log ("Camera Size1 : " + Camera.main.orthographicSize);
				/*
				Screen.autorotateToLandscapeLeft = true;
				Screen.autorotateToLandscapeRight = true;
				Screen.autorotateToPortrait = false;
				Screen.autorotateToPortraitUpsideDown = false;
				*/
			//Screen.orientation = ScreenOrientation.AutoRotation;
		} else {
			Screen.orientation = ScreenOrientation.Portrait;
			Debug.Log ("HorizontalCamera Ori2 : " + Screen.orientation.ToString());
			aspectScript.setAspect (0);
			Debug.Log ("Camera Size2 : " + Camera.main.orthographicSize);
			/*
				Screen.autorotateToLandscapeLeft = false;
				Screen.autorotateToLandscapeRight = false;
				Screen.autorotateToPortrait = true;
				Screen.autorotateToPortraitUpsideDown = false;

			*/
			//Screen.orientation = ScreenOrientation.AutoRotation;
		}
	}

}
