﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {
	private Cat cat;
	public int points;

	// Use this for initialization
	void Start () {
		cat = GameObject.FindWithTag ("Player")
			.GetComponent<Cat> ();
	}

	void Collect() {
		if (cat != null) {
			cat.Collect (this);
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.CompareTag ("Ball")) {
			Collect ();
			Destroy (this.gameObject); // Or transform?
		}
	}
}
