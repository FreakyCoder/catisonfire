﻿using UnityEngine;
using System.Collections;

public static class Constants
{

	// PlayerPref Keys
	public const string PLAYER_ID = "player_id";
	public const string PLAYER_NAME = "player_name";
	public const string PLAYER_EMAIL = "player_email";
	public const string LOGIN_CHECK = "login_check";
	public const string PLAYER_VOLUME = "player_volume";
	public const string PLAYER_MUSIC = "player_music";
	public const string PLAYER_LAN = "player_lan";
	public const string CURRENT_LEVELID = "current_levelid";
	public const string CURRENT_LEVELMAX = "current_levelmax";
	public const string CURRENT_SCORE = "current_score";




}
