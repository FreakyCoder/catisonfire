﻿using System.Collections;
using System.Collections.Generic;
using System;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Scripting;

public class MainBallScript : MonoBehaviour
{
	private float startY;
	private float startX;
	public static GameObject itemBeingDragged;
	public Rigidbody2D rb2d;


	//Getting ball's starting coordinates
	private void Awake()
	{
		startY = transform.localPosition.y;
		startX = transform.localPosition.x;

		itemBeingDragged = gameObject;
		rb2d = itemBeingDragged.GetComponent<Rigidbody2D> ();
	}


	//when script is enabled
	private void OnEnable()
	{
		
		GetComponent<MetaGesture> ().TouchBegan += beganHandler;
		GetComponent<MetaGesture> ().TouchMoved += movedHandler;
		GetComponent<MetaGesture>().TouchEnded+= endedHandler;
		GetComponent<MetaGesture>().TouchCancelled+= cancelledHandler;

	}

	//when script is disabled
	private void OnDisable()
	{ 	
		GetComponent<MetaGesture> ().TouchBegan -= beganHandler;
		GetComponent<MetaGesture> ().TouchMoved -= movedHandler;
		GetComponent<MetaGesture>().TouchEnded-= endedHandler;
		GetComponent<MetaGesture>().TouchCancelled-= cancelledHandler;
	}


	//ball drag began
	private void beganHandler(object sender, EventArgs e)
	{
		if (GameAliveScript.isGameAlive == 0) {
			//first position of the ball
			transform.localPosition = transform.position;
	
			//ball and it's rigidbody is assigned
			itemBeingDragged = gameObject;
			rb2d = itemBeingDragged.GetComponent<Rigidbody2D> ();

			//ball's rigidbody is adjusted to avoid physics bugs
			rb2d.gravityScale = 0;
			rb2d.isKinematic = true;
			rb2d.freezeRotation = true;
			rb2d.velocity = new Vector2 (0f, 0f);
		}
	}

	//ball is being dragged
	private void movedHandler(object sender, EventArgs e)
	{	
		/*if (GameAliveScript.isGameAlive == 0) {
			//changing ball's position
			transform.localPosition = new Vector3 (Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position).x, startY, 0); 
		}*/
	}

	//drag cancelled
	private void cancelledHandler(object sender, EventArgs e)
	{	
		
	}

	//drag ended
	private void endedHandler(object sender, EventArgs e)
	{
		if (GameAliveScript.isGameAlive == 0) {
			//ball's rigidbody is turned back to normal when drag is done
			rb2d.gravityScale = 1;
			rb2d.isKinematic = false;
			rb2d.freezeRotation = false;
			rb2d.velocity = new Vector2 (0f, 0f);
			GameAliveScript.isGameAlive = 1;
			GetComponent<MainBallScript> ().enabled = false;
		}
	}

}
