﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.InputSources;
using TouchScript;
using UnityEngine;
using UnityEngine.EventSystems;
public class SlotScript : MonoBehaviour, IDropHandler {
	public GameObject itemDropped;
	//checking if there is an item on the selected slot 
	public GameObject item{
		get{
			//if there is, assign it to "item",else return null
			if (transform.childCount > 0) {
				return transform.GetChild (0).gameObject;
			}
			return null;
		}
	}
	#region IDropHandler implementation
	//drag is dropped on a slot
	public void OnDrop (PointerEventData eventData)
	{





		string itemName = eventData.pointerDrag.ToString();
		string name = itemName;
		var script = eventData.pointerDrag.GetComponent<ToolbarDragHandler> ();
		var script2 = eventData.pointerDrag.GetComponent<ToolbarDragHandlerBox> ();
		var script3 = eventData.pointerDrag.GetComponent<ToolbarDragHandlerTahterevalli> ();
		var script4 = eventData.pointerDrag.GetComponent<ToolbarDragHandlerBomb> ();
		var script5 = eventData.pointerDrag.GetComponent<ToolbarDragHandlerPolygon> ();
		itemDropped = GameObject.Find(eventData.pointerDrag.name);


		//var script2 = itemDropped.GetComponent<ToolbarDragHandlerBox> ();
		if (script != null) {
			Debug.Log ("script1!=");
			int active = script.getActivated();
			if (active == 1) {
				itemDropped.transform.position = script.getStartPosition();
				Debug.Log ("slotscript: active1");}
		} else if (script2 != null) {
			Debug.Log ("script2!=");
			int active2 = script2.getActivated ();
			if (active2 == 1) {
				itemDropped.transform.position = script2.getStartPosition ();
				Debug.Log ("slotscript: active2");
			}
		}else if(script3 != null){
			Debug.Log ("script3!=");
			int active3 = script3.getActivated ();
			if (active3 == 1) {
				itemDropped.transform.position = script3.getStartPosition ();
				Debug.Log ("slotscript: active3");
			}
		}else if(script4 != null){
			Debug.Log ("script4!=");
			int active4 = script4.getActivated ();
			if (active4 == 1) {
				itemDropped.transform.position = script4.getStartPosition ();
				Debug.Log ("slotscript: active4");
			}
		}else if(script5 != null){
			Debug.Log ("script5!=");
			int active5 = script5.getActivated ();
			if (active5 == 1) {
				itemDropped.transform.position = script5.getStartPosition ();
				Debug.Log ("slotscript: active5");
			}
		}
	}
	#endregion
}