﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ToolbarDragHandlerPolygon : MonoBehaviour, IBeginDragHandler,IDragHandler,IEndDragHandler {
	public static GameObject itemBeingDragged;
	public static GameObject invisParent;
	public static GameObject mainBall;
	public static GameObject countdownTextObject;
	public Rigidbody2D rb2d;
	public PolygonCollider2D pc2d;
	private GameObject scoreHolder;
	public Vector3 startPosition;
	private int activated = 0;
	Transform startParent;

	void Start(){
		scoreHolder = GameObject.Find("ScoreHolder");
		invisParent = GameObject.Find("InvisiblePanel"); //invisiblePanel is assigned
		countdownTextObject = GameObject.Find("CountdownText"); // Countdown text assigned
		mainBall = GameObject.Find("AsteroidSprite"); // AsteroidSprite is assigned
		itemBeingDragged = gameObject; //dragged item is assigned
		pc2d = itemBeingDragged.GetComponent<PolygonCollider2D>();//collider is assigned to avoid unnecessary collision
	}

	void Update(){
		if(countdownTextObject.GetComponent<CountdownScript>().countdownTimer<=0 || GameAliveScript.isGameAlive == 1){
			pc2d.enabled = true;
			if (activated == 1)
				pc2d.isTrigger = false;
		}
	}

	#region IBeginDragHandler implementation

	//Toolbar item begins to drag
	public void OnBeginDrag (PointerEventData eventData)
	{	 var scoreScript = scoreHolder.GetComponent<ScoreScript> ();
		//pc2d.isTrigger = true;
		if(GameAliveScript.isGameAlive == 0){ //check if game started
			if (activated == 0) {
				scoreScript.toolUsed ();
			}
			activated = 1;

			pc2d.enabled=false;

			startPosition = transform.position; //first position of dragged item is assigned
			startParent = transform.parent; //parent of dragged item is assigned
			GetComponent<CanvasGroup> ().blocksRaycasts = false; //disallow collision
		}else{
			//pc2d.isTrigger = false;
		}
	}

	#endregion

	#region IDragHandler implementation

	//Toolbar item is being dragged, in process
	public void OnDrag (PointerEventData eventData)
	{
		if (GameAliveScript.isGameAlive == 0) { //check if game started

			//pc2d.isTrigger = true;

			Vector3 screenPoint;
			screenPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition); // mouse input
			screenPoint.z = 30.0f; //set z manually for visual

			//check the distance between the dragged item and the ball to stop collision/bug
			if (screenPoint.y < mainBall.transform.position.y + 1.3f && screenPoint.x < mainBall.transform.position.x +1.3f && screenPoint.y > mainBall.transform.position.y - 1.3f && screenPoint.x > mainBall.transform.position.x -1.3f){
				if(screenPoint.x >0)
					screenPoint.y = mainBall.transform.position.y + 1.3f;
				screenPoint.x = mainBall.transform.position.x + 1.3f;

			}


			transform.position = screenPoint;// change item's position


		} else {

			//pc2d.isTrigger = false;
		}
	}

	#endregion

	#region IEndDragHandler implementation

	//Toolbar item drag ends
	public void OnEndDrag (PointerEventData eventData)
	{
		if (GameAliveScript.isGameAlive == 0) { //check if game started
			pc2d.enabled = true;
			itemBeingDragged = null; // Item is not being dragged anymore
			GetComponent<CanvasGroup> ().blocksRaycasts = true; //allow collision
			//pc2d.isTrigger = true;

			if (transform.parent == startParent) { //check if the parent is changed
				transform.SetParent (invisParent.transform);  //if not set it to invisParent panel slot
			}

			//activated = 0;
		} else {
			//pc2d.isTrigger = false;
		}


	}
	#endregion

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("ontriggerenter2d");
	}

	public int getActivated(){
		return activated;
	}

	public Vector3 getStartPosition(){
		return startPosition;
	}



}
