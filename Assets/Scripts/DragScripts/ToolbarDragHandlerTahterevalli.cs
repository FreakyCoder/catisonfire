﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ToolbarDragHandlerTahterevalli : MonoBehaviour, IBeginDragHandler,IDragHandler,IEndDragHandler {

	public static GameObject itemBeingDragged;
	public static GameObject invisParent;
	public static GameObject mainBall;
	public static GameObject countdownTextObject;
	public Rigidbody2D rb2d;
	public BoxCollider2D bc2d,bc2dChild;
	public PolygonCollider2D pc2d;
	public Vector3 startPosition;
	private int activated = 0;
	private int firstDrag =0;
	private GameObject scoreHolder;
	Transform startParent;
	[SerializeField] Transform plank;

	void Start(){
		scoreHolder = GameObject.Find("ScoreHolder");
		invisParent = GameObject.Find("InvisiblePanel"); //invisiblePanel is assigned
		countdownTextObject = GameObject.Find("CountdownText"); // Countdown text assigned
		mainBall = GameObject.Find("AsteroidSprite"); // AsteroidSprite is assigned
		itemBeingDragged = gameObject; //dragged item is assigned
		bc2d = itemBeingDragged.GetComponent<BoxCollider2D>();//collider is assigned to avoid unnecessary collision
		pc2d = itemBeingDragged.GetComponent<PolygonCollider2D>(); //collider is assigned to avoid unnecessary collision
		bc2dChild = GetComponentsInChildren<BoxCollider2D>()[1];

	}

	void Update(){
		if(countdownTextObject.GetComponent<CountdownScript>().countdownTimer<=0 || GameAliveScript.isGameAlive == 1){
			bc2d.enabled = true;
			pc2d.enabled = true;
			bc2dChild.enabled = true;
			if (activated == 1){
				bc2d.isTrigger = false;
				bc2dChild.isTrigger = false;
				pc2d.isTrigger = false;
			}
		}
	}

	#region IBeginDragHandler implementation

	//Toolbar item begins to drag
	public void OnBeginDrag (PointerEventData eventData)
	{	
		var scoreScript = scoreHolder.GetComponent<ScoreScript> ();
		plank.SetParent (transform);

		if(GameAliveScript.isGameAlive == 0){ //check if game started
			if(firstDrag==0)
				transform.localScale = new Vector3(0.8F, 0.8F, 0);
			firstDrag = 1;

			if (activated == 0) {
				scoreScript.toolUsed ();
			}
			activated = 1;

			bc2d.enabled=false;
			bc2dChild.enabled = false;
			pc2d.enabled = false;

			startPosition = transform.position; //first position of dragged item is assigned
			startParent = transform.parent; //parent of dragged item is assigned
			GetComponent<CanvasGroup> ().blocksRaycasts = false; //disallow collision
		}
	}

	#endregion

	#region IDragHandler implementation

	//Toolbar item is being dragged, in process
	public void OnDrag (PointerEventData eventData)
	{
		if (GameAliveScript.isGameAlive == 0) { //check if game started

			Vector3 screenPoint;
			screenPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition); // mouse input
			screenPoint.z = 30.0f; //set z manually for visual

			//check the distance between the dragged item and the ball to stop collision/bug
			if (screenPoint.y < mainBall.transform.position.y + 1.3f && screenPoint.x < mainBall.transform.position.x +1.3f && screenPoint.y > mainBall.transform.position.y - 1.3f && screenPoint.x > mainBall.transform.position.x -1.3f){
				if(screenPoint.x >0)
					screenPoint.y = mainBall.transform.position.y + 1.3f;
				screenPoint.x = mainBall.transform.position.x + 1.3f;
			}
			transform.position = screenPoint;// change item's position
		}
	}

	#endregion

	#region IEndDragHandler implementation

	//Toolbar item drag ends
	public void OnEndDrag (PointerEventData eventData)
	{

		if (GameAliveScript.isGameAlive == 0) { //check if game started
			bc2dChild.enabled = true;
			pc2d.enabled = true;

			itemBeingDragged = null; // Item is not being dragged anymore
			GetComponent<CanvasGroup> ().blocksRaycasts = true; //allow collision

			if (transform.parent == startParent) { //check if the parent is changed
				transform.SetParent (invisParent.transform);  //if not set it to invisParent panel slot
			}

			plank.SetParent (transform.parent);
		} 
	}
	#endregion

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("ontriggerenter2d");
	}

	public int getActivated(){
		return activated;
	}

	public Vector3 getStartPosition(){
		return startPosition;
	}

}
