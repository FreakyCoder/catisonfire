﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	public GameObject ExplosionGo;
	public int keepEffect=0;
	public Collider2D other2;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if (GameAliveScript.isGameAlive == 1 && keepEffect == 1) {
			PlayExplosion ();
			Destroy (gameObject);
			Destroy (other2.gameObject, 0.5f);

		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("Enemy")) {
			keepEffect = 1;
			other2 = other;
			if (GameAliveScript.isGameAlive == 1) {
				PlayExplosion ();
				Destroy (gameObject);
				Destroy (other.gameObject, 0.5f);


		}
		}

	}

	void OnTriggerExit2D(Collider2D other){
		keepEffect = 0;
		other2 = null;
		Debug.Log("on trigger exit calisti");
	}



	void PlayExplosion(){
		GameObject explosion = (GameObject)Instantiate (ExplosionGo);

		explosion.transform.position = transform.position;
	}
		
}
