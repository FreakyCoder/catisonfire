﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPush : MonoBehaviour {
	public GameObject ExplosionGoPush;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("Tiles")) {
			Destroy (gameObject);
			PlayExplosionPush ();

		}
	}

	void PlayExplosionPush(){
		GameObject explosionPush = (GameObject)Instantiate (ExplosionGoPush);

		explosionPush.transform.position = transform.position;
	}
}
