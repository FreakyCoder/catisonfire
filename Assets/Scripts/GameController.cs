﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public static GameController instance;
	public static int login_check = 0;

	void Awake(){
		if (instance == null) {
			DontDestroyOnLoad (this);
			Debug.Log ("DDOL : " + gameObject.name);
		}

		instance = this;
		Debug.Log ("Awake : " + gameObject.name);
	}

	public void setLoginCheck(int status){
		login_check = status;
	}

	public int LoginCheck(){
		return login_check;
	}
}
