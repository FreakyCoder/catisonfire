﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrolScript : MonoBehaviour {

	public float walkSpeed;      // Walkspeed
	public float wallLeft;       // Define wallLeft
	public float wallRight;      // Define wallRight
	float walkingDirection = -1.0f;
	Vector2 walkAmount;
	float originalX; // Original float value
	public SpriteRenderer sRen;

	void Start () {
		this.originalX = this.transform.position.x;
		sRen = GetComponent<SpriteRenderer> ();
			
	}

	// Update is called once per frame
	void Update () {
		walkAmount.x = walkingDirection * walkSpeed * Time.deltaTime;

		if (walkingDirection > 0.0f && transform.position.x >= wallRight) {
			walkingDirection = -1.0f;
			sRen.flipX = false;

		} else if (walkingDirection < 0.0f && transform.position.x <= wallLeft) {
			walkingDirection = 1.0f;
			sRen.flipX = true;
		}
		transform.Translate(walkAmount);
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag ("Bomb")) {
			walkSpeed = 0;
			sRen.flipX = false;
		}

	}
		
}
