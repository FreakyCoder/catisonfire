﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {

	public static GameObject levelBall;
	public static GameObject countdownTextObject;
	public GameObject scoreHolder;
	public GameObject Popup_GameOver;
	public GameObject scoreText;

	// Use this for initialization
	void Start () {
		
		levelBall = GameObject.Find ("AsteroidSprite");
		countdownTextObject=GameObject.Find("CountdownText");
		scoreHolder = GameObject.Find ("ScoreHolder");
	}
	
	// Update is called once per frame
	void Update () {
		var countdownScript = countdownTextObject.GetComponent<CountdownScript> ();
		if (countdownScript.roundTimer <= 0) {
			Time.timeScale = 0;
			GameAliveScript.isGameAlive = 0;
			levelBall.gameObject.SetActive (false);
			Popup_GameOver.SetActive (true);
		}
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Wheel")){
			var scoreScript = scoreHolder.GetComponent<ScoreScript>();
			int score = scoreScript.calculateScore(false);

			Debug.Log ("score-: "+ score);

			GameAliveScript.isGameAlive = 0;
			Time.timeScale = 0;
			levelBall.gameObject.SetActive (false);
			Popup_GameOver.SetActive (true);
			scoreText = GameObject.Find ("LevelScore");
			var scoreTextScript = scoreText.GetComponent<SetScoreTextScript> ();
			scoreTextScript.setScoreText(score);
		}
	}
}
