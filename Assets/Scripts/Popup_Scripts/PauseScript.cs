﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseScript : MonoBehaviour {


	public GameObject Popup_GO;	// Pop_GO Holder Object
	public GameObject Popup_GameOver;
	public GameObject Popup_GameClear;

	void Awake()
	{
		// Deactivating pop-up
		Popup_GO.SetActive (false);
		Popup_GameOver.SetActive (false);
		Popup_GameClear.SetActive (false);
	}

	public void popupWindow(){
		// On/off pop-up
		if (Popup_GO.active) {
			Popup_GO.SetActive (false);

		} else {
			Popup_GO.SetActive (true);
			Popup_GameOver.SetActive (false);
			Popup_GameClear.SetActive (false);
		}
			
	}

	public void popupGame(){
		if (Popup_GameOver.active) {
			Popup_GameOver.SetActive (false);
		} else {
			Popup_GameOver.SetActive (true);
		}

		if (Popup_GameClear.active) {
			Popup_GameClear.SetActive (false);
		} else {
			Popup_GameClear.SetActive (true);
		}
	}

}
