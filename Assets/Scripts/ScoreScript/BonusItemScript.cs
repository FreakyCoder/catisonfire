﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusItemScript : MonoBehaviour {

	public int points;
	private GameObject scoreHolder;

	void OnTriggerEnter2D(Collider2D collider){
		scoreHolder = GameObject.Find ("ScoreHolder");
		var scoreScript = scoreHolder.GetComponent<ScoreScript>();
		scoreScript.bonusCollected(points);
		Debug.Log ("bonus collected points: " + points);
		Destroy (gameObject);
	}
}
