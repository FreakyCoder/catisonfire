﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScoreScript : MonoBehaviour {

	static private int totalScore=0;
	private int LevelID;
	private int maxScore;
	private int timeVal =50; // value of how much each second will increase the score
	private int totalBonus; //value that user gathers from collecting coins and cookies
	public int toolCost; //value of how much points it costs to use a tool during solution
	private int toolsUsed; //number of tools used
	private int bonusItemCollected; //number of bonus items collected
	private GameObject cdText;

	// Use this for initialization
	void Start () {
		toolsUsed = 0;
		bonusItemCollected = 0;
		cdText = GameObject.Find ("CountdownText");
		LevelID = PlayerPrefs.GetInt(Constants.CURRENT_LEVELID);
		maxScore = PlayerPrefs.GetInt (Constants.CURRENT_LEVELMAX);
		Debug.Log ("score: " + maxScore + "levelID: " + LevelID);
	}

	// Update is called once per frame
	void Update () {
		
	}

	public int calculateScore(bool levelSucceeded){
	
		totalScore = 0;
		if(GameAliveScript.isGameAlive==1){

		int timeScore=0;
		var countdownScript = cdText.GetComponent<CountdownScript>();
		float timeLeft = countdownScript.getRoundTimer (); // timeleft when the game is finished

		timeScore = Convert.ToInt32 (timeLeft) * timeVal;

			Debug.Log ("toolsUsed1: " + toolsUsed);
			totalScore = totalScore + timeScore - toolCost*toolsUsed + totalBonus;

			PlayerPrefs.SetInt(Constants.CURRENT_SCORE, totalScore);
		
			if (levelSucceeded == true) {
				PlayerPrefs.SetInt(Convert.ToString(LevelID), totalScore);
				Debug.Log ("PlayerPref:" + PlayerPrefs.GetInt(Convert.ToString(LevelID)) + "LevelID:" + LevelID);
			}


		}

		return totalScore;
	}

	public void bonusCollected(int scoreEarned){
		totalBonus = totalBonus + scoreEarned;
		Debug.Log ("bonusCollected fonk: " + scoreEarned +" total: " + totalScore);
		bonusItemCollected = bonusItemCollected + 1;
	}

	public void toolUsed(){
		toolsUsed = toolsUsed + 1;
		Debug.Log ("tool used, score lowered by: " + toolCost);
		Debug.Log ("toolsUsed2: " + toolsUsed);
	}

	public void resetToolsUsed(){
		toolsUsed = 0;
		bonusItemCollected = 0;
		Debug.Log ("Tool sayisi sifirlandi");
		Debug.Log ("toolsUsed3: " + toolsUsed);
	}
}
