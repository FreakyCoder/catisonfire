﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SettingType  {
	Volume = 0,
	Music = 1
}

public class ConditionalSpriteScript : MonoBehaviour {
	public SettingType settingType;
	public Sprite active;
	public Sprite passive;
	public UnityEngine.UI.Image target;
		
	public void Reinvoke() {
		bool condition = false;
		switch (settingType) {
		case SettingType.Music:
			condition = SettingsUtility.GameMusic;
			break;
		case SettingType.Volume:
			condition = SettingsUtility.ClickSound;
			break;
		default:
			break;
		}

		if (condition) {
			target.sprite = active;
		} else {
			target.sprite = passive;
		}
	}

	void Start () {
		Reinvoke ();
	}
}