﻿//Generate for constant variables for languages
public enum Language
{
    Turkish = 0,
    English = 1,
    Spanish = 2
}

//Get languages from game with getter and setter methods
public class Vocabulary
{
    public Language Language { get; set; }
    public Word[] Words { get; set; }

    public Vocabulary()
    {
    }

    public Vocabulary(Language language, Word[] words)
    {
        Language = language;
        Words = words;
    }
}

//Get words from game related to languages with getter and setter methods
public class Word
{
    public string Id { get; set; }
    public string Value { get; set; }

    public Word()
    {
    }

    public Word(string id, string value)
    {
        Id = id;
        Value = value;
    }
}