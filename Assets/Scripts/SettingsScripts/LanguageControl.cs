﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Get and check the wors related to languages
public class LanguageControl : MonoBehaviour
{
	public string key = null;
	public string placeHolder = null;

    void Start()
    {
		var text = GetComponentInChildren<Text> ();

		if (text == null)
			return;
		
		string keyToUse = key;

		// Set language texts
		if (string.IsNullOrEmpty(keyToUse)) {
			keyToUse = text.text;
		}

		if (string.IsNullOrEmpty (keyToUse)) {
			return;
		}

		if (string.IsNullOrEmpty(placeHolder)) {
			text.text = SettingsUtility.GetWord (keyToUse);
		} else {
			text.text = text.text.Replace (placeHolder, SettingsUtility.GetWord (keyToUse));
		}
    }
}