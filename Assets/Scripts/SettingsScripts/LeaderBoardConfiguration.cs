﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerRanking {
	First = 0,
	Second = 1,
	Third = 2,
	Other = 3
}

public class LeaderBoardConfiguration : MonoBehaviour {
	public Sprite profileBackground;
	public Sprite topPlayerProfileForeground;
	public Sprite otherPlayerProfileForeground;
	public Sprite firstProfileCrown;
	public Sprite secondProfileCrown;
	public Sprite thirdProfileCrown;
	public Sprite background;
	public Sprite splitter;

	public Sprite GetProfileBackgroundFor(PlayerRanking ranking) {
		return profileBackground;
	}

	public Sprite GetProfileForegroundFor(PlayerRanking ranking) {
		return ranking == PlayerRanking.Other ?
			otherPlayerProfileForeground :
			topPlayerProfileForeground;
	}

	public Sprite GetProfileCrownFor(PlayerRanking ranking) {
		switch (ranking) {
		case PlayerRanking.First:
			return firstProfileCrown;
		case PlayerRanking.Second:
			return secondProfileCrown;
		case PlayerRanking.Third:
			return thirdProfileCrown;
		default:
			return null;
		}
	}

	public Sprite GetBackgroundFor(PlayerRanking ranking) {
		return background;
	}

	public Sprite GetSplitter() {
		return splitter;
	}
}