﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoardControl : MonoBehaviour {
    public int PADDING = 115;
    public Font font;
    public LeaderBoardDataProvider dataProvider;
	public LeaderBoardConfiguration configuration;
	public GameObject prefab;
	private long currentPlayer = 10212180759652798;

	public long CurrentPlayer
	{ 
		get { return this.currentPlayer; } 
		set {
			this.currentPlayer = value;

			LoadRankings ();
		} 
	}

	public void setCurrentPlayer(string id) {
		long lid;
		if (long.TryParse (id, out lid)) {
			CurrentPlayer = lid;
		}
	}
   
    // Use this for initialization
    void Start()
    {
		if (dataProvider == null || configuration == null)
            return;

		dataProvider.RankingsLoad += RankingsLoaded;

		LoadRankings ();
    }

	private void LoadRankings() {
		if (dataProvider == null || configuration == null)
			return;

		dataProvider.SetCurrentPlayer (this.currentPlayer);

		dataProvider.LoadRankings ();
	}

	private void RankingsLoaded(object sender, RankingsResponse response) {
		var currentPlayerRanking = response.CurrentPlayerRanking;

		if (currentPlayerRanking != null) {
			var currentPlayerLabel = GameObject.Find ("lblCurrentPlayerRank");
			var currentPlayerLabelText = currentPlayerLabel.GetComponent<Text> ();
			var prefix = SettingsUtility.GetWord ("yourRankIs");
			currentPlayerLabelText.text = prefix + " " + currentPlayerRanking.rank.ToString ();

			//  Facebook Code for "Welcome, ..."
			//var prefix = SettingsUtility.GetWord ("welcome");
			//WELCOMEUSERTEXT.text = prefix + " " + CURRENTUSERNAME;
		}

		var parentTop3 = GameObject.Find("ContentTop3");
		var parentOthers = GameObject.Find("ContentOthers");
		if (parentTop3 == null || parentOthers == null)
			return;

		foreach (Transform child in parentTop3.transform) {
			Destroy (child.gameObject);
		}

		foreach (Transform child in parentOthers.transform) {
			Destroy (child.gameObject);
		}

		var rankings = response.Rankings;

		// TOP 3
		for (int i = 0; i < ((rankings.Length > 3) ? 3 : rankings.Length); i++)
			Put(parentTop3.transform, rankings[i], i);

		// Other players
		for (int i = 3; i < rankings.Length; i++)
			Put (parentOthers.transform, rankings [i], i);
	}

    private GameObject Put(Transform parent, Ranking ranking, int i)
    {
        var player = ranking.player;
        var rank = ranking.rank;
        var playerRanking = ranking.PlayerRanking;

        var obj = Instantiate(prefab);
        obj.transform.SetParent(parent);

        var script = obj.GetComponent<PlayerRankingScript>();
        script.Background = configuration.GetBackgroundFor(playerRanking);
        script.ProfileBackground = configuration.GetProfileBackgroundFor(playerRanking);
        script.ProfileForeground = configuration.GetProfileForegroundFor(playerRanking);
        script.Crown = configuration.GetProfileCrownFor(playerRanking);
        script.PlayerName = player.name;
        script.PlayerRank = player.score.ToString();

        return obj;
    }
}
