﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

/* */
public class PlayerDto
{
	private long id;
	private string name;
	private int score;

	public long Id { get { return id; } set { id = value; } } 
	public string Name { get { return name; } set { name = value; } }
	public int Score { get { return score; } set { score = value; } }

	public Player ToPlayer() {
		return new Player {
			id = this.id,
			name = this.name,
			score = this.score
		};
	}
}
/* */
public class Ranking
{
	public Player player;

	public int rank;

	public PlayerRanking PlayerRanking {
		get
		{
			return rank >= 1 && rank <= 3 ?
				(PlayerRanking)(rank - 1) :
				PlayerRanking.Other;
		}
	}
}

class CiofResponse : EventArgs {
	public string response;

	public CiofResponse (string response) : base()
	{
		this.response = response;
	}
}

public class RankingsResponse : EventArgs {
	private readonly Ranking currentPlayerRanking;
	private readonly Ranking[] rankings;
	public Ranking CurrentPlayerRanking { get { return currentPlayerRanking;} }
	public Ranking[] Rankings { get { return rankings; } }

	public RankingsResponse (Ranking currentPlayerRanking, Ranking[] rankings)
	{
		this.currentPlayerRanking = currentPlayerRanking;
		this.rankings = rankings;
	}
}

delegate void DataLoadEventHandler(object sender, CiofResponse response);

public delegate void RankingsLoadEventHandler(object sender, RankingsResponse response);

//Fill the leaderboard with texts
public class LeaderBoardDataProvider : MonoBehaviour {
	private long? currentPlayerId;
	public string URL = "http://freakycoder.net/php/leaderboard.php";
	public string playerSeperator = "&&";
	public string attributeSeperator = ";";

	public event RankingsLoadEventHandler RankingsLoad;
	
	public LeaderBoardDataProvider() 
	{
	}

	private IEnumerator Post() 
	{
		WWWForm form = new WWWForm();
		form.AddField("limit", "10");

		UnityWebRequest www = UnityWebRequest.Post(URL, form);
		yield return www.Send();

		if(www.isError) {
			Debug.Log(www.error);
		}
		else {
			Debug.Log("Form upload complete!");
		}
	}

	private IEnumerator Get(DataLoadEventHandler handler) 
	{
		UnityWebRequest www = UnityWebRequest.Get(URL);
		yield return www.Send();

		if(!www.isError) {
			var text = www.downloadHandler.text;
			handler (this, new CiofResponse (text));
		}
	}

	/* */
	public void SetCurrentPlayer(long id)
	{
		currentPlayerId = id;
	}

	/* */
	public void UpdateScore(int score)
	{
		// TODO: Goto the web method to update current player's (currentPlayer.Id) score (score)
	}

	public void LoadRankings() {
		StartCoroutine (Get (RankingsLoaded)); 
		// StartCoroutine (Post (RankingsLoaded)); şeklinde
		// LeaderBoardControl.CurrentPlayer = playerId; Reload Page
	}

	private void RankingsLoaded(object sender, CiofResponse response) {
		response.response = response.response.Replace ("\n", null);
		var scores = response.response.Split (new string[] { playerSeperator }, StringSplitOptions.RemoveEmptyEntries);

		var players = new List<PlayerDto> ();
		for (int i = 0; i < scores.Length; i++) {
			var playerData = scores [i].Split (new string[] { attributeSeperator }, StringSplitOptions.RemoveEmptyEntries);

			if (playerData.Length != 3) {
				continue;
			}

			long id;
			int score;
			if (long.TryParse (playerData [0], out id) &&
				int.TryParse (playerData [2], out score)) {
				players.Add(new PlayerDto {
					Id = id,
					Name = playerData [1],
					Score = score
				});
			}
		}

		var sortedPlayers = players.OrderByDescending (x => x.Score).ToList ();
		var rankings = new Ranking[sortedPlayers.Count];
		for (int i = 0; i < rankings.Length; i++) {
			rankings [i] = new Ranking {
				player = sortedPlayers[i].ToPlayer(),
				rank = i + 1
			};
		}

		Ranking currentPlayerRanking = null;
		if (currentPlayerId.HasValue) {
			currentPlayerRanking = rankings.FirstOrDefault (x => x.player.id == currentPlayerId.Value);
		}

		RankingsLoaded (new RankingsResponse (currentPlayerRanking, rankings));
	}

	private void RankingsLoaded(RankingsResponse response) {
		if (RankingsLoad != null) {
			RankingsLoad.Invoke (this, response);
		}
	}
}
