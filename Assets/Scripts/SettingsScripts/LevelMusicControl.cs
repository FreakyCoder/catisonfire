﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// DEPRECATED
public class LevelMusicControl : MonoBehaviour
{
    public AudioClip clip;

    // Use this for initialization
    void Start()
    {
        //Play the music during game
        MusicControl.Instance.play(clip);
    }

    public void update()
    {
        if (!MusicControl.Flag)
        {
            return;
        }

        MusicControl.Instance.update();
    }
}