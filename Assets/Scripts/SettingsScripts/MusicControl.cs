﻿using System;
using UnityEngine;

//Generate the music check class
public class MusicControl : MonoBehaviour
{
    //Create the global object for game's music
    private static MusicControl instance = null;
    public static MusicControl Instance
    {
        get { return instance; }
    }

    private static bool flag;
    public static bool Flag { get { return flag; } }

    public AudioSource source;
    public AudioClip music;

    // Load the music
    void Awake()
    {
        if (flag)
        {
            Destroy(source);
            return;
        }

        //Load the whether to be music
        if (instance == null) 
        {
            instance = this;
            flag = true;
        }

        //Avoid from the stopping the music
		DontDestroyOnLoad(this.gameObject);

        //Call the update function
        update();
    }

    // Update the game music
    public void update()
    {
        if (source == null)
            return;
        //Deafult clip
        if (source.clip == null)
            source.clip = music;

		source.loop = true;

        //Whether to tap music button (off)
        if (!SettingsUtility.GameMusic && source.isPlaying)
            source.Stop();
        else if (SettingsUtility.GameMusic && !source.isPlaying) // //Whether to tap music button (on)
            source.Play();
    }

    //Level-1 Scene includes different levels, each level has their music and when players change or complete the level music will change
    public void play(AudioClip clip)
    {
        //Whether to control music on or off
        if (!SettingsUtility.GameMusic)
            return;
        source.Stop();
        source.clip = clip;
        source.Play();
    }
}