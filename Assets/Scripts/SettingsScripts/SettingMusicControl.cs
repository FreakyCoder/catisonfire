﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingMusicControl : MonoBehaviour {
    // Update the game music
    public void update()
    {
        if (!MusicControl.Flag)
        {
            return;
        }

        MusicControl.Instance.update();
    }

}
