﻿using System;
using UnityEngine;

//Check the game's settings related to sound, music and languages
public class SettingsControl : MonoBehaviour {
	
    //Check to change music
    public void changeGameMusicState()
    {
        SettingsUtility.GameMusic = !SettingsUtility.GameMusic;
    }

    //Check to change sound
    public void changeClickSoundState()
    {
        SettingsUtility.ClickSound = !SettingsUtility.ClickSound;
    }

    //Check to change languages
    public void changeLanguage(Language language)
    {
        SettingsUtility.Language = language;
    }

    //Check to change languages to parse
    public void changeLanguage(string language)
    {
        try
        {
            var lang = (Language)Enum.Parse(typeof(Language), language);
            
            SettingsUtility.Language = lang;
        }
        catch
        {
        }
    }
}