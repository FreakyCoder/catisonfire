﻿using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public static class SettingsUtility
{
	#region Defaults
	private const bool GAME_MUSIC_DEFAULT = true;
	private const bool CLICK_SOUND_DEFAULT = true;
	private const Language LANGUAGE_DEFAULT = Language.English;


	private class KeywordPairs
	{
		private readonly string key;
		private readonly string[] values;

		public string Key { get { return key; } }

		public KeywordPairs (string key, params string[] values)
		{
			this.key = key;
			this.values = values;
		}

		public string getValueFor(Language language) {
			return values [(int)language];
		}
	}

	private static class KeywordPairsArrayUtility
	{
		public static Vocabulary[] GetVocabularies (KeywordPairs[] array) {
			var enumValues = Enum.GetValues (typeof(Language));
			var vocabularies = new Vocabulary[enumValues.Length];
			int i = 0;
			foreach (Language language in enumValues) {
				var words = new Word[array.Length];
				for (int j = 0; j < array.Length; j++) {
					words [j] = new Word (array [j].Key,
						array [j].getValueFor (language));
				}
				vocabularies [i++] = new Vocabulary (language,
					words);
			}

			return vocabularies;
		}
	}

	private static readonly KeywordPairs[] pairs = new KeywordPairs[]
	{
		new KeywordPairs("language", "DIL", "LANGUAGE", "IDIOMA"),
		new KeywordPairs("about", 	"HAKKINDA", "ABOUT", "ASUNTO"),
		new KeywordPairs("settings", "Ayarlar", "Settings", "Ajuste"),
		new KeywordPairs("sound", 	"Ses", "Sound", "Sonido"),
		new KeywordPairs("yourRankIs", "Seviyeniz", "Your rank is", "Tu nivel"),
		new KeywordPairs("selectLevel", "Bölüm Seçiniz", "Select Level", "Selecciona el Nivel"),
		new KeywordPairs("levelCompleted", "Bulmacayı Çözdün", "Puzzle Solved", "Nivel Completado"),
		new KeywordPairs("yourScore", "Puan", "Your Score", "Puntuacion"),
		new KeywordPairs("failed", "Az daha oluyordu\n Hadi bir daha dene", "Almost done\n Let's try again", "Casi\nIntenta de nuevo"),
		new KeywordPairs("save", "Kaydet", "Save", "grabar"),
		new KeywordPairs("gamePaused", "Oyun duraklatıldı", "Game Paused", "Pausa"),
		new KeywordPairs("collected", "toplandı", "collected", "recogido"),
		new KeywordPairs("coins", "sikke", "coins", "monedas"),
		new KeywordPairs("cookies", "kurabiye", "cookies", "galletas"),
		new KeywordPairs("bonus_items", "Bonus eşya", "Bonus items", "Artículos de bonificación"),
		new KeywordPairs("retry", "Tekrar", "Retry", "De nuevo"),
		new KeywordPairs("resume", "Devam", "Resume", "Continua"),
		new KeywordPairs("music", "Müzik", "Music", "Musica"),
		new KeywordPairs("next", "İleri", "Next", "Siguiente"),
		new KeywordPairs("welcome", "Hoşgeldin", "Welcome", "Bienvenido"),
		new KeywordPairs("shareFacebook", "Facebook'da Paylaş", "Share Facebook", "Compartir en Facebook"),
	};

	//Get the languages' words
	private static Vocabulary[] _VOCABS_DEFAULT;
	private static Vocabulary[] VOCABS_DEFAULT {
		get{
			if (_VOCABS_DEFAULT == null)
				_VOCABS_DEFAULT = KeywordPairsArrayUtility.GetVocabularies (pairs);

			return _VOCABS_DEFAULT;
		}
	}

	#endregion

	#region Keys

	public const string GAME_MUSIC_KEY = "GameMusic";
	public const string CLICK_SOUND_KEY = "ClickSound";
	public const string LANGUAGE_KEY = "Language";

	#endregion

	#region Values

	//Check game music when tap
	public static bool GameMusic {
		get {
			if (!PlayerPrefs.HasKey (GAME_MUSIC_KEY)) {
				PlayerPrefs.SetString (GAME_MUSIC_KEY, GAME_MUSIC_DEFAULT.ToString ());
				return GameMusic; // Start the Get logic over
				// Or alternatively return the default value.
				// return GAME_MUSIC_DEFAULT;
			}

			var value = PlayerPrefs.GetString (GAME_MUSIC_KEY);
			if (string.IsNullOrEmpty (value)) {
				PlayerPrefs.SetString (GAME_MUSIC_KEY, GAME_MUSIC_DEFAULT.ToString ());
				return GameMusic; // Start the Get logic over
				// Or alternatively return the default value.
				// return GAME_MUSIC_DEFAULT;
			}

			bool gameMusic;
			if (!bool.TryParse (value, out gameMusic)) {
				PlayerPrefs.SetString (GAME_MUSIC_KEY, GAME_MUSIC_DEFAULT.ToString ());
				return GameMusic; // Start the Get logic over
				// Or alternatively return the default value.
				// return GAME_MUSIC_DEFAULT;
			}

			return gameMusic;
		}
		set {
			PlayerPrefs.SetString (GAME_MUSIC_KEY, value.ToString ());
		}
	}

	//Check game sound when tap
	public static bool ClickSound {
		get {
			if (!PlayerPrefs.HasKey (CLICK_SOUND_KEY)) {
				PlayerPrefs.SetString (CLICK_SOUND_KEY, CLICK_SOUND_DEFAULT.ToString ());
				return ClickSound; // Start the Get logic over
				// Or alternatively return the default value.
				// return CLICK_SOUND_DEFAULT;
			}

			var value = PlayerPrefs.GetString (CLICK_SOUND_KEY);
			if (string.IsNullOrEmpty (value)) {
				PlayerPrefs.SetString (CLICK_SOUND_KEY, CLICK_SOUND_DEFAULT.ToString ());
				return ClickSound; // Start the Get logic over
				// Or alternatively return the default value.
				// return CLICK_SOUND_DEFAULT;
			}

			bool clickSound;
			if (!bool.TryParse (value, out clickSound)) {
				PlayerPrefs.SetString (CLICK_SOUND_KEY, CLICK_SOUND_DEFAULT.ToString ());
				return ClickSound; // Start the Get logic over
				// Or alternatively return the default value.
				// return CLICK_SOUND_DEFAULT;
			}

			return clickSound;
		}
		set {
			PlayerPrefs.SetString (CLICK_SOUND_KEY, value.ToString ());
		}
	}

	//Check game languages when tap
	public static Language Language {
		get {
			if (!PlayerPrefs.HasKey (LANGUAGE_KEY)) {
				PlayerPrefs.SetString (LANGUAGE_KEY, LANGUAGE_DEFAULT.ToString ());
				return Language; // Start the Get logic over
				// Or alternatively return the default value.
				// return LANGUAGE_DEFAULT;
			}

			var value = PlayerPrefs.GetString (LANGUAGE_KEY);
			if (string.IsNullOrEmpty (value)) {
				PlayerPrefs.SetString (LANGUAGE_KEY, LANGUAGE_DEFAULT.ToString ());
				return Language; // Start the Get logic over
				// Or alternatively return the default value.
				// return LANGUAGE_DEFAULT;
			}

			Language language;
			try {
				language = (Language)Enum.Parse (typeof(Language), value);
			} catch {
				PlayerPrefs.SetString (CLICK_SOUND_KEY, CLICK_SOUND_DEFAULT.ToString ());
				return Language; // Start the Get logic over
				// Or alternatively return the default value.
				// return LANGUAGE_DEFAULT;
			}

			return language;
		}
		set {
			if (!Enum.IsDefined (typeof(Language), value))
				return; // Should not occur, throw an exception maybe

			PlayerPrefs.SetString (LANGUAGE_KEY, value.ToString ());
		}
	}

	#endregion

	#region Methods

	private enum ReadMode
	{
		Static = 0,
		File = 1
	}

	// This section will be fixed as file
	private static readonly ReadMode READ_MODE = ReadMode.Static;

	private static Vocabulary GetVocabulary(Language language, ReadMode mode = ReadMode.Static) {
		var vocabularies = GetVocabularies (mode);
		for (int i = 0; i < vocabularies.Length; i++) {
			if (vocabularies [i].Language == language) {
				return vocabularies [i];
			}
		}

		return null;
	}

	private static Vocabulary[] GetVocabularies(ReadMode mode = ReadMode.Static) {
		if (mode == ReadMode.File) {
			var serializer = new XmlSerializer (typeof(Vocabulary[]));

			var path = Path.Combine (Application.persistentDataPath, "globalization.xml");

			if (!File.Exists (path)) {
				CreateGlobalizationFile ();
				FillGlobalizationFile ();
			}

			using (var stream = new FileStream (path, FileMode.Open)) {
				try {
					return serializer.Deserialize (stream) as Vocabulary[];
				} 
				catch (Exception e) {
					stream.Close ();
					FillGlobalizationFile ();
				} 
				finally {
					stream.Close ();
				}
			}
		}

		return VOCABS_DEFAULT;
	}

	//Get words from game with file
	public static string GetWord (string id)
	{
		var vocabulary = GetVocabulary (Language, READ_MODE);

		if (vocabulary == null) {
			return null;
		}

		var idLower = id.ToLower ();

		string word = null;
		for (int i = 0; i < vocabulary.Words.Length; i++) {
			if (vocabulary.Words [i].Id.ToLower () == idLower) {
				word = vocabulary.Words [i].Value;
				break;
			}
		}

		return word;
	}

	//Create the file for words in game
	private static void CreateGlobalizationFile ()
	{
		var path = Path.Combine (Application.persistentDataPath, "globalization.xml");

		if (File.Exists (path))
			return;

		File.Create (path)
            .Close ();
	}

	//Fill the file with game's words
	private static void FillGlobalizationFile ()
	{
		var path = Path.Combine (Application.persistentDataPath, "globalization.xml");

		var serializer = new XmlSerializer (typeof(Vocabulary[]));

		using (var stream = new FileStream (path, FileMode.Truncate)) {
			try {
				serializer.Serialize (stream, VOCABS_DEFAULT);
			} catch (Exception e) {
				Console.WriteLine (e);
			} finally {
				stream.Close ();
			}
		}
	}

	#endregion
}