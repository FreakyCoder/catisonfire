﻿using System.Threading;
using UnityEngine;

//Generate sound check class
public class SoundControl : MonoBehaviour {
	public AudioSource source;
	public AudioClip click;

    private void Awake()
    {
        //DontDestroyOnLoad(source);
        // DontDestroyOnLoad(click);
    }

    //Update to click the game sound
    public void onClick()
    {
        if (source == null)
            return;
        
        if (source.clip == null)
            source.clip = click;

        if (SettingsUtility.ClickSound)
            source.Play();
    }
}