﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using System;

public class FBScript : MonoBehaviour {

	GameController gameController = new GameController();

	public GameObject DialogLoggedIn;   // FB Login Button
	public GameObject DialogLoggedOut;	// Logout Button
	public GameObject DialogUsername;	// Username Label
	public GameObject Cookies;			// Cookies BG

	public GameObject loading;

	private string user_id;
	private string username;
	private string email;
	private string first_name;
	private string last_name;

	User user = new User();

	Dictionary<string,object> FBUserDetails = new Dictionary<string,object>();

	void Awake()
	{
		// Facebook Initialization
		FB.Init (SetInit, OnHideUnity);
	}

	void Start(){
		if (gameController.LoginCheck () == 1) {
			DisableFBLoginButton ();
			DisplayUsername ();
		} else {
			EnableFBLoginButton ();
		}
	}

	void SetInit()
	{
		// Checking FB is logged in or not
		if (FB.IsLoggedIn) {
			Debug.Log ("FB is logged in");
		} else {
			Debug.Log ("FB is not logged in");
		}

		DealWithFBMenus (FB.IsLoggedIn);
	}

	// Hiding the game when FB login dialog is open
	void OnHideUnity(bool isGameShown)
	{
		// Stopping time when game is hidden
		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}

	public void FBlogin()
	{
		// Loading Indicator Starts
		loading.SetActive(true);
		FB.LogInWithReadPermissions (new List<string>(){"public_profile", "email"}, AuthCallBack);
	}

	// Callback method from FB's read permissions
	void AuthCallBack(IResult result)
	{
		if (result.Error != null) {
			Debug.Log (result.Error);
		} else {
			if (FB.IsLoggedIn) {
				Debug.Log ("FB is logged in");
			} else {
				Debug.Log ("FB is not logged in");
			}

			// Dealing with FB 
			DealWithFBMenus (FB.IsLoggedIn);
		}

	}

	void DealWithFBMenus(bool isLoggedIn)
	{
		
		// Handling UI depends on FB logged in
		if (isLoggedIn) {
			DisableFBLoginButton ();

			// Getting user's FB Information
			FB.API("/me?fields=id,name,email", HttpMethod.GET, FetchProfileCallback, new Dictionary<string,string>(){});

		} else {
			EnableFBLoginButton ();
		}
	}

	private void FetchProfileCallback (IGraphResult result) {

		Debug.Log (result.RawResult);

		FBUserDetails = (Dictionary<string,object>)result.ResultDictionary;

		user_id = Convert.ToString(FBUserDetails ["id"]);
		username = Convert.ToString(FBUserDetails ["name"]);

		var names = username.Split(' ');
		first_name = names[0];
		last_name = names[1];
		email = Convert.ToString(FBUserDetails ["email"]);


		StartCoroutine (insertDB());

		Debug.Log ("Profile: id: " + FBUserDetails["id"]);
		Debug.Log ("Profile: username: " + FBUserDetails["name"]);
		Debug.Log ("Profile: email: " + FBUserDetails["email"]);

	}
		
	IEnumerator insertDB(){
		if (FB.IsLoggedIn) {
			yield return new WaitForSeconds (0.1f);
			string url = "http://freakycoder.net/php/register.php";

			Debug.Log("postFBRegister : " + user_id +"\n"+ username +"\n"+email);

			WWWForm form = new WWWForm();
			// Sending the POST values
			form.AddField("user_id", user_id);
			form.AddField("username", username);
			form.AddField("email", email);
			// Sending the request
			WWW www = new WWW(url, form);
			// Response from the server
			StartCoroutine (responseWWW (www));
		}
	}

	IEnumerator getUserInfo(){
			
		yield return new WaitForSeconds (0.1f);
		// Ouz,ouz@ciof.com,1,1,Spanish,101,1000,102,1500,103,550,105,1560
		// username, e-mail, music, vol, language, levelID, levelScore
		string url= "http://freakycoder.net/php/getUserInfo.php";

		WWWForm form = new WWWForm ();
		form.AddField ("user_id", user_id);

		WWW www = new WWW (url, form);

		StartCoroutine (responseWWWgetUserInfo (www));
	}

	IEnumerator responseWWW(WWW www){

		yield return www;
		Debug.Log ("Registered Value : "+www.text);

		switch (Convert.ToInt16(www.text)) {
		case 0: // 0 means user is already exist 
			StartCoroutine(getUserInfo());
			break;
		case 1 : // 1 means registration is completed successfully
			setPlayerPref(); // Set the logged in user's player prefs
			Debug.Log("User is registered!, logged in successfully!");
			break;
		default : 
			PlayerPrefs.SetInt (Constants.LOGIN_CHECK, 0);
			Debug.Log("WWW Returned the default part, check the error!");
			break;
		}

		setPlayerPref(); // Set the logged in user's player prefs
		// Display the logged in FB username & email
		DisplayUsername ();
		gameController.setLoginCheck (1);


		// Loading Indicator Ends
		loading.SetActive(false);

		yield break;
	}

	IEnumerator responseWWWgetUserInfo(WWW www){
		yield return www;

		Debug.Log ("Full : " + www.text);

		if (www.text == "0") {
			Debug.Log ("response WWW get user info = 0");
		}else{
		string[] split = www.text.Split (',');
		user.Username = split[0];
		user.EMail = split[1];
		user.Music = Convert.ToInt32(split [2]);
		user.Volume = Convert.ToInt32(split [3]);
		user.Language = split[4];

		string[] split2 = split[5].Split(';');
			for (int i = 0; i < split2.Length-1; i++) {
				Debug.Log ("SPLIT 2 : " + split2 [i]);
			}
		setUserInfo(user, split, split2);
		
		}
			
		// Loading Indicator Ends
		loading.SetActive(false);
	}

	private void setUserInfo(User user, string[] split, string[] split2){
		PlayerPrefs.SetInt (Constants.LOGIN_CHECK, 1);
		PlayerPrefs.SetString(Constants.PLAYER_ID, user_id);
		PlayerPrefs.SetString (Constants.PLAYER_NAME, user.Username);
		PlayerPrefs.SetString (Constants.PLAYER_EMAIL, user.EMail);
		PlayerPrefs.SetInt (Constants.PLAYER_VOLUME, user.Volume);
		PlayerPrefs.SetInt (Constants.PLAYER_MUSIC, user.Music);
		PlayerPrefs.SetString (Constants.PLAYER_LAN, user.Language);
		int k = 0;
		for (int i = 0; i < (split2.Length-1)/2; i++) {
			int levelID;
			int levelScore;
		
			int.TryParse (split2 [k], out levelID);
			int.TryParse (split2 [k + 1], out levelScore);
			Level level = new Level (levelID, levelScore);
			user.LevelList.Add(level);
			PlayerPrefs.SetInt (Convert.ToString(level.LevelID), levelScore);
			k = k + 2;

			Debug.Log("Level ID:" + level.LevelID);
			Debug.Log("Level Score:" + PlayerPrefs.GetInt(Convert.ToString(level.LevelID)));	
		}
	}

	private void setPlayerPref(){
		PlayerPrefs.SetInt (Constants.LOGIN_CHECK, 1);
		PlayerPrefs.SetString(Constants.PLAYER_ID, user_id);
		PlayerPrefs.SetString (Constants.PLAYER_NAME, username);
		PlayerPrefs.SetString (Constants.PLAYER_EMAIL, email);
	}

	// Setting FB accouns's username to the username lbl
	void DisplayUsername()
	{
		StopCoroutine (insertDB());

		Debug.Log ("First Name : " + first_name);
		Debug.Log ("Email : " + email);
		Text usernameTxt = DialogUsername.GetComponent<Text> ();

		usernameTxt.text = string.Format("Welcome <color=#693B00FF>{0}</color>", first_name); 
		// Disable FB Login Button and Enable Logout Button
		DisableFBLoginButton ();
	}

	// FB logout button
	public void Logout(){
		if (FB.IsLoggedIn)
		{                                                                                  
			FB.LogOut ();
			StartCoroutine ("CheckForSuccussfulLogout");
		} 
	}

	IEnumerator CheckForSuccussfulLogout()
	{
		if (FB.IsLoggedIn) 
		{
			yield return new WaitForSeconds (0.1f);
			StartCoroutine ("CheckForSuccussfulLogout");
		} else 
		{
			Debug.Log ("FB is logged out successfully!");

			PlayerPrefs.DeleteAll ();
			gameController.setLoginCheck (0);
			// Here you have successfully logged out.
			// Do whatever you want as I do, I just enabled Login Button and Disabled
			// logout button through this method.
			EnableFBLoginButton ();
		}
	}

	public void EnableFBLoginButton(){
		DialogLoggedIn.SetActive (false);
		DialogLoggedOut.SetActive (true);
		DialogUsername.SetActive (false);
		Cookies.SetActive (false);
	}

	public void DisableFBLoginButton(){
		DialogLoggedIn.SetActive (true);
		DialogLoggedOut.SetActive (false);
		DialogUsername.SetActive (true);
		Cookies.SetActive (true);
	}	
}