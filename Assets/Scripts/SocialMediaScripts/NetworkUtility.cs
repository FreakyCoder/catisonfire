﻿using UnityEngine;

public class NetworkUtility {
    public static bool CanConnect() {
		return Application.internetReachability != NetworkReachability.NotReachable;
    }
}
