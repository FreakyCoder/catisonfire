﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelStarScript : MonoBehaviour {

	public int maxScore;
	public int levelID;
	bool flag = false;
	public Sprite star0;
	public Sprite star1;
	public Sprite star2;
	public Sprite star3;
	Image myImage;


	void Start () {
		myImage = GetComponent<Image> ();
		flag = false;
		checkUnlocked ();
		calculateStars ();
	}

	void calculateStars(){
		int playerScore = PlayerPrefs.GetInt (Convert.ToString (levelID));
		if (playerScore != 0) {
			if (playerScore < maxScore / 3) { //0-199
				myImage.sprite = star1;
			} else if (playerScore >= maxScore / 3 && playerScore < maxScore / 3 * 2) { //200-399
				myImage.sprite = star2;
			} else if (playerScore >= maxScore / 3 * 2 && playerScore < maxScore) { //400-599
				myImage.sprite = star3;
			} else if (playerScore >= maxScore) {
				myImage.sprite = star3;
			}

			if (flag == true) {
				myImage.sprite = star0;
			}
		}
	}

	void checkUnlocked(){
		int unlockVal; // 0-locked, 1-unlocked, 2-lastlevelplayed
		//int checkID = Convert.ToInt32("101");  //player preften id
		int score = PlayerPrefs.GetInt(Convert.ToString(levelID));

		Debug.Log ("SCORE : " + score);
		if (levelID == 101) {
			unlockVal = 1;
			GetComponentInParent<LevelUnlockScript> ().unlockLevel (unlockVal);
		} else if (score != 0) {
			unlockVal = 1;
			GetComponentInParent<LevelUnlockScript> ().unlockLevel (unlockVal);
		}
		else{

			int lvlID = levelID;
			lvlID = lvlID - 1;
			Debug.Log ("LEVEL ID : " + lvlID);
			if (PlayerPrefs.GetInt (Convert.ToString (lvlID)) != 0) {
				flag = true;
				Debug.Log ("LEVEL ID ENTERED: " + lvlID);
				unlockVal = 1;
				GetComponentInParent<LevelUnlockScript> ().unlockLevel (unlockVal);
			} else {

				Debug.Log ("LEVEL ID NOT ENTERED: " + lvlID);
				unlockVal = 0;
				GetComponentInParent<LevelUnlockScript> ().unlockLevel (unlockVal);
			}
		}
	}

	public int getMaxScore(){
		return maxScore;
	}

	public int getLevelID(){
		return levelID;
	}

}
