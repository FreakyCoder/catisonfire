﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelUnlockScript : MonoBehaviour {

	public Sprite unlockedImage;
	Button interactBtn;
	Image myImage;

	void Start () {
		//myImage = GetComponent<Image> ();
	}


	public void unlockLevel(int unlock){
		myImage = GetComponent<Image> ();
		interactBtn = GetComponent<Button> ();
		if (unlock == 1) {
			interactBtn.interactable = true;
			//unlock set
			myImage.sprite = unlockedImage;
		}
		else if(unlock == 0){
			interactBtn.interactable = false;
			//locked set	
		}

	}

}
