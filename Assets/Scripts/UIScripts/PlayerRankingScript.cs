﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRankingScript : MonoBehaviour {

	public GameObject playerRankingPrefab;

	public Sprite Background {
		get{
			var obj = Get<Image> ("Background");
			return obj.sprite;
		}
		set{
			var obj = Get<Image> ("Background");
			obj.sprite = value;
		}
	}

	public Sprite ProfileBackground {
		get{
			var obj = Get<Image> ("ProfileBackground");
			return obj.sprite;
		}
		set{
			var obj = Get<Image> ("ProfileBackground");
			obj.sprite = value;
		}
	}

	public Sprite ProfileForeground {
		get{
			var obj = Get<Image> ("ProfileForeground");
			return obj.sprite;
		}
		set{
			var obj = Get<Image> ("ProfileForeground");
			obj.sprite = value;
		}
	}

	public Sprite Crown {
		get{
			var obj = Get<Image> ("Crown");
			return obj.sprite;
		}
		set{
			var obj = Get<Image> ("Crown");
			obj.sprite = value;

			if (value == null) {
				obj.color = new Color (0, 0, 0, 0);
			}
		}
	}

	public string PlayerName {
		get{
			var obj = Get<Text> ("LabelName");
			return obj.text;
		}
		set{
			var obj = Get<Text> ("LabelName");
			obj.text = value;
		}
	}

	public string PlayerRank {
		get{
			var obj = Get<Text> ("LabelRank");
			return obj.text;
		}
		set{
			var obj = Get<Text> ("LabelRank");
			obj.text = value;
		}
	}

	private T Get<T>(string name) {
		var child = playerRankingPrefab.transform.Find (name);
		if (child == null) {
			return default(T);
		}

		return child.GetComponent<T> ();
	}
}
