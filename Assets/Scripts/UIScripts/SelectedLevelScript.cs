﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SelectedLevelScript : MonoBehaviour {

	private int maxScore;
	private int levelID;

	public void setOnClick(){
		var lsrScript = gameObject.GetComponentInChildren<LevelStarScript>();

		maxScore = lsrScript.maxScore;
		levelID = lsrScript.levelID;


		PlayerPrefs.SetInt (Constants.CURRENT_LEVELID, levelID);
		PlayerPrefs.SetInt (Constants.CURRENT_LEVELMAX, maxScore);

	}

}
