﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SetCompleteStarScript : MonoBehaviour {

	public Sprite star1;
	public Sprite star2;
	public Sprite star3;

	Image myImage;


	public void setCompletedStar(){
		myImage = GetComponent<Image> ();

		int levelID = PlayerPrefs.GetInt (Constants.CURRENT_LEVELID);
		int currentScore = PlayerPrefs.GetInt (Constants.CURRENT_SCORE);
		int maxScore = PlayerPrefs.GetInt (Constants.CURRENT_LEVELMAX);

		Debug.Log ("maxScore: " + maxScore);
		Debug.Log ("levelID: " + levelID);
		if (currentScore < maxScore / 3) { //0-199
			myImage.sprite = star1;
			Debug.Log ("1");
		} else if (currentScore >= maxScore / 3 && currentScore < maxScore / 3 * 2) { //200-399
			myImage.sprite = star2;
		} else if (currentScore >= maxScore / 3 * 2 && currentScore < maxScore) { //400-599
			myImage.sprite = star3;
		} else if (currentScore >= maxScore) {
			myImage.sprite = star3;
		}
		Debug.Log ("current score: " + currentScore);
	}

}

