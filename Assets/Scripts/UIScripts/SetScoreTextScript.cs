﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SetScoreTextScript : MonoBehaviour {

	private Text scoreText;

	public void setScoreText(int score){
		scoreText = GetComponent<Text> ();
		scoreText.text = Convert.ToString (score);
	}
}
