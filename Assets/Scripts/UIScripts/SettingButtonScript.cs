﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SettingButtonScript : MonoBehaviour
{

	int login_check = 0;
	string user_id;
	int player_music;
	int player_volume;
	string player_lan;

	public void getPlayerPref(){
		login_check = PlayerPrefs.GetInt (Constants.LOGIN_CHECK);
		user_id = PlayerPrefs.GetString (Constants.PLAYER_ID);
		Debug.Log ("Setting Button Login Check : " + login_check);

		if (login_check == 1) {
			// Get the user settings from server
			getUserSettings ();

		} else {
			// Set the default settings, if user is not logged in
			setPlayerPrefs (1,1,"English");
		}
	}

	void setPlayerPrefs(int music, int volume, string lan){
		PlayerPrefs.SetInt (Constants.PLAYER_MUSIC, music);
		PlayerPrefs.SetInt (Constants.PLAYER_VOLUME, volume);
		PlayerPrefs.SetString (Constants.PLAYER_LAN, lan);
	}


	void getUserSettings(){
		string url = "http://freakycoder.net/php/get_setting.php";

			Debug.Log ("getUserSettings userID : " + user_id);
			WWWForm form = new WWWForm();
			// Sending the POST values
			form.AddField("user_id", user_id);
			// Sending the request
			WWW www = new WWW(url, form);
			// Response from the server
			StartCoroutine (responseWWW (www));
	}

	IEnumerator responseWWW(WWW www){
		yield return www;
		Debug.Log ("getSetting Response : "+www.text);

		if (PlayerPrefs.GetInt (Constants.PLAYER_MUSIC) == 0) {
			string[] arr = www.text.Split (',');
			setPlayerPrefs (Convert.ToInt16(arr [0]), Convert.ToInt16(arr [1]), arr [2]);
		} 

		LoadScene ();

	}

	// Load the SettingScene
	void LoadScene(){
		SceneManager.LoadScene ("SettingScene");
		Time.timeScale = 1;
	}
}

