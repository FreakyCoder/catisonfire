﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuCtrl : MonoBehaviour {

	// Load the new scene via sceneName
	public void LoadScene(string sceneName){
		SceneManager.LoadScene (sceneName);
		Time.timeScale = 1;
	}
}
