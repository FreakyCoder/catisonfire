﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartSceneScript : MonoBehaviour {

	public GameObject scoreHolder;

	public void RestartScene(){
		scoreHolder = GameObject.Find ("ScoreHolder");
		var scoreScript = scoreHolder.GetComponent<ScoreScript> ();
		scoreScript.resetToolsUsed ();

		Time.timeScale = 1;
		Application.LoadLevel (Application.loadedLevel);
	}
}
